import json
import os

from slack import RTMClient
from db import select_app, update_app, init_db


def find_pod(user: str):
    app, namespace = select_app(user)
    if app is not None:
        pod, result = get_pod(app, namespace)
        return {
            'namespace': namespace,
            'app': app,
            'pod': pod
        }
    return None


def pods_command(conv, tokens):
    if len(tokens) > 0:
        user = tokens[0]
    else:
        user = f'<@{conv.user}>'  # command sender

    result = find_pod(user)
    if result is None:
        return f"No app has been set for {user} "  # yeah, error handling should be better here

    return pods(result['app'], result['namespace'])


def logs_command(conv, tokens):
    if len(tokens) > 0:
        user = tokens[0]
    else:
        user = f'<@{conv.user}>'  # command sender

    result = find_pod(user)
    if result is None:
        return f"No app has been set for {user} "  # yeah, error handling should be better here

    return logs(result['app'], result['namespace'])


def extractor_command(conv, tokens):
    if len(tokens) > 0:
        user = tokens[0]
    else:
        user = f'<@{conv.user}>'  # command sender

    result = find_pod(user)
    if result is None:
        return f"No app has been set for {user} "  # yeah, error handling should be better here

    return 'Executing extractor...', extractor(result['pod'], result['app'], result['namespace'])


def visits_command(conv, tokens):
    if len(tokens) > 0:
        user = tokens[0]
    else:
        user = f'<@{conv.user}>'  # command sender

    result = find_pod(user)
    if result is None:
        return f"No app has been set for {user} "  # yeah, error handling should be better here

    return 'Executing visits downloader...', visits(result['pod'], result['app'], result['namespace'])


def get_app_command(conv, tokens):
    if len(tokens) > 0:
        user = tokens[0]
    else:
        user = f'<@{conv.user}>'  # command sender

    result = find_pod(user)
    if result is not None:
        return f"Current app for {user} is {result['app']}"
    return f"No app has been set for {user}"


def set_app_command(conv, tokens):
    if len(tokens) < 2: #< 3:
        return 'Unable to understand, please message with set-app <@user> <application>' #<namespace>'
    else:
        user = tokens[0]
        app = tokens[1]
        namespace = "default"
        #namespace = tokens[2]

        set_context(app)

        pod, result = get_pod(app, namespace)
        if len(result['items'][0]['spec']['containers']) > 1:
            return f'Application {app} has multiple containers'  # todo: allow container selection
        update_app(app, namespace, user)

        return f"Successfully set app for {user} to {app}"


def set_context(app):
    print("Configuring kube context")
    if "staging" in app:
        print("Switching to Staging context")
        cmd = f'kubectl config use-context tacter-staging'
        print(f'Executing {cmd}')
        stream = os.popen(cmd)
        return stream.read()
    else:
        print("Switching to Production context")
        cmd = f'kubectl config use-context tacter-prod'
        print(f'Executing {cmd}')
        stream = os.popen(cmd)
        return stream.read()


def get_pod(app, namespace):
    cmd = f'kubectl get pods --selector=app={app} --namespace={namespace} -ojson'
    print(f'Executing {cmd}')
    stream = os.popen(cmd)
    result = json.loads(stream.read())
    if len(result['items']) == 0:
        raise Exception(f'{app} not found in {namespace}')
    pod = result['items'][0]['metadata']['name']
    return pod, result


def pods(app, namespace):
    cmd = f'kubectl get pods --selector=app={app} --namespace={namespace}'
    print(f'Executing {cmd}')
    stream = os.popen(cmd)
    return stream.read()


def logs(app, namespace):
    cmd = f'kubectl logs --selector=app={app} --namespace={namespace}'
    print(f'Executing {cmd}')
    stream = os.popen(cmd)
    return stream.read()


def extractor(pod, app, namespace):
    if "lol" in app:
        print("Running extractor in LOL")
        cmd = f'kubectl --namespace={namespace} exec -ti {pod} -- npm run start:lol:extractor'
        print(f'Executing {cmd}')
        stream = os.popen(cmd)
        return stream.read()
    elif "tft" in app:
        print("Running extractor in TFT")
        cmd = f'kubectl --namespace={namespace} exec -ti {pod} -- npm run start:tft:extractor'
        print(f'Executing {cmd}')
        stream = os.popen(cmd)
        return stream.read()
    else:
        print("This {pod} service does not have _extractor_ option")


def visits(pod, app, namespace):
    if "service" in app:
        print("Running visits-downloader in TACTER-SERVICE")
        cmd = f'kubectl --namespace={namespace} exec -ti {pod} -- npm run start:amplitude:guide-visits'
        print(f'Executing {cmd}')
        stream = os.popen(cmd)
        return stream.read()
    else:
        print("This {pod} service does not have _visits-downloader_ option")


commands = {
    'set-app': set_app_command,
    'get-app': get_app_command,
    'pods': pods_command,
    'logs': logs_command,
    'extractor': extractor_command,
    'visits': visits_command
}


class Conversation:
    def __init__(self, web_client, channel, user):
        self.web_client = web_client
        self.channel = channel
        self.user = user

    def msg(self, text):
        # self.web_client.chat_postEphemeral(
        #     channel= self.channel,
        #     user=self.user,
        #     text=text,
        #
        # )
        self.web_client.chat_postMessage(
            channel=self.channel,
            text=text,

        )


welcome = '''
Hi <@{user}>. I'm your Tacter ChatOps bot.
Use _{me} set-app @user service_ to set the service for a user
Use _{me} get-app @user_ to get the current selected service for the user 
Use _{me} pods_ to list the pods in the selected service 
Use _{me} logs_ to get the logs in the selected service 
Use _{me} extractor_ to run extractor in the selected service 
Use _{me} visits_ to run visits-downloader in the selected service 
'''


@RTMClient.run_on(event="message")  # subscribe to 'message' events
def process_command(**payload):
    data = payload['data']
    web_client = payload['web_client']
    print(payload)
    # ignore service messages, like joining a channel
    is_service = 'subtype' in data and data['subtype'] is not None

    if not is_service and 'text' in data and '<@U0661QBLVF0>' in data['text']:
        channel_id = data['channel']
        thread_ts = data['ts']
        user = data['user']
        text = data['text']  # get data from the event
        tokens = text.split()  # split it up by space characters
        me = tokens[0]  # user id of the chat bot
        # object to track the conversation state
        conv = Conversation(web_client, channel_id, user)
        if len(tokens) > 1:
            print(tokens)
            # first token is my userid, second will be the command e.g. logs
            command = tokens[1]
            print('received command ' + command)
            if command in commands:
                # get the actual command executor
                command_func = commands[command]
                try:
                    args = tokens[slice(2, len(tokens))]
                    # execute the command
                    result = command_func(conv, args)
                    if result is not None:
                        # and return the value from the
                        # command back to the user
                        conv.msg(result)
                except Exception as e:
                    conv.msg(str(e))

            else:
                # show welcome message
                web_client.chat_postMessage(
                    conv.msg(welcome.format(user=user, me=me))
                )
        else:
            # show welcome message
            conv.msg(welcome.format(user=user, me=me))


def main():
    init_db()
    slack_token = os.environ["SLACK_API_TOKEN"]
    rtm_client = RTMClient(token=slack_token)
    rtm_client.start()


if __name__ == "__main__":
    main()
